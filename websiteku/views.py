from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import jadwalKegiatan
from .models import Schedule

response = {'author': "Anastasia Greta"}

def index1(request):
    return render(request, 'index1.html')

def index2(request):
	return render(request, 'index2.html')

def index3(request):
	return render(request, 'index3.html')

def forms(request):
	html = 'forms.html'
	response['fill_form'] = jadwalKegiatan
	return render(request, html, response)

def fill_form(request):
	# form = jadwalKegiatan(request.POST or None)
	if (request.method == 'POST'):
		response['name'] = request.POST['nama_kegiatan'] 
		response['place'] = request.POST['tempat'] 
		response['category'] = request.POST['kategori'] 
		response['date'] = request.POST['tanggal']
		jadwal = Schedule(nama_kegiatan=response['name'], tempat=response['place'], 
							kategori=response['category'], tanggal=response['date'])
		jadwal.save()
		html = 'forms.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/websiteku/')

def table_form(request):
	if request.method == "POST":
		jadwal = Schedule.objects.all().delete()
		return HttpResponseRedirect('/table')
	else:
		jadwal = Schedule.objects.all()
		response['jadwal'] = jadwal
		html = 'table.html'
		return render(request, html , response)