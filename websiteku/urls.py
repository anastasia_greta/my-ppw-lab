from django.conf.urls import url
from .views import index1, index2, index3, forms, fill_form, table_form
#url for app
urlpatterns = [
    url('index1', index1),
	url('index2', index2),
	url('index3', index3),
	url('forms', forms),
	url('fill_form', fill_form, name='fill_form'),
	url('table', table_form),
	url('', index1)
]
