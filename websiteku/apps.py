from django.apps import AppConfig


class WebsitekuConfig(AppConfig):
    name = 'websiteku'
