from django import forms

class jadwalKegiatan(forms.Form):
    # error_message = {
    #     'required' : 'salah'
    #     'invalid' : 'masih salah'
    # }
    attrs  = {
        'class' : 'form-control'
    }

    nama_kegiatan = forms.CharField(label="Activity Name", required=True, max_length=50, empty_value="Anonymous", widget=forms.TextInput(attrs=attrs))
    kategori = forms.CharField(label="Category", required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    tempat = forms.CharField(label="Place", required=True, max_length=50, widget=forms.TextInput(attrs=attrs))
    tanggal = forms.DateTimeField(label="Date", required=True, widget=forms.DateTimeInput(attrs={'type':'datetime-local', 'class':'form-control'}))