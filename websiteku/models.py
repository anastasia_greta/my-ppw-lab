from django.db import models
from django.utils import timezone

class Schedule(models.Model):
    nama_kegiatan = models.CharField(max_length=50)
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)
    tanggal = models.DateTimeField() 
